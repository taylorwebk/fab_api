<?php
namespace Controllers;

use \Models\Response as Resp;
use \Models\Utils;
use \Models\Root;
use \Models\Admin;

class RootController
{
  public static function update($admin, $data) {
    if ($admin->tipo != 'root') {
      $response = Resp::badRequest('Actualizar datos del root solo con token del root');
      return $response;
    }
    if (array_key_exists('correo', $data['datos'])) {
      $checkAdmin = Admin::where('correo', $data['datos']['correo'])->first();
      if ($checkAdmin) {
        $resp = Resp::unauthorized(
          'correo enviado es igual que el correo del un administrativo',
          'El correo ya esta siendo utilizado'
        );
        return $resp;
      }
    }
    if (array_key_exists('password', $data['datos'])) {
      $data['datos']['password'] = password_hash($data['datos']['password'], PASSWORD_DEFAULT);
    }
    $root = Utils::getRoot();
    $root->update($data['datos']);
    $root->tipo = 'root';
    $response   = Resp::okWhitToken(
      'Datos actualizados',
      'Datos guardados',
      Utils::generateToken($admin->id, $admin->correo, $admin->tipo),
      ['user' => $root]
    );
    return $response;
  }
}
