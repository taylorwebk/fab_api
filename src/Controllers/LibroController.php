<?php
namespace Controllers;

use \Models\Utils;
use \Models\Response as Resp;
use \Models\Libro;

class LibroController
{
  public static function list($admin) {
    if ($admin->tipo != 'administrativo') {
      $response = Resp::badRequest('Lista de libros solo con token del administrativo');
      return $response;
    }
    $libros = Libro::all();
    $resp   = Resp::okWhitToken(
      'libros cargados',
      'Lista de libros obtenida',
      Utils::generateTokenFromAdmin($admin),
      ['libros' => $libros]
    );
    return $resp;
  }
}