<?php
namespace Controllers;

use \Models\Response as Resp;
use \Models\Utils;
use \Models\Estudiante;
use \Models\Curso;
use \Models\Cursa;

use \Controllers\EstudianteController;

class CursaController
{
  // campos requeridos: curso_id, nombres,
  // apellidos, ci, sexo, cel y correo
  public static function inscribe($admin, $data) {
    $curso = Curso::find($data['datos']['curso_id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe el Curso con id: ' . $data['datos']['curso_id']);
      return $resp;
    }
    $est = Estudiante::where('ci', $data['datos']['ci'])->first();
    if ($est) {
      $resp = Resp::unauthorized('Ya existe el ci', 'El ci: ' . $data['datos']['ci'] . ' ya fue registrado.');
      return $resp;
    }
    $estudianteData = [
      'nombres'   => $data['datos']['nombres'],
      'apellidos' => $data['datos']['apellidos'],
      'ci'        => $data['datos']['ci'],
      'sexo'      => $data['datos']['sexo'],
      'cel'       => $data['datos']['cel'],
      'correo'    => $data['datos']['correo']
    ];

    $estudiante = Estudiante::create($estudianteData);
    $fecha      = date('Y-m-d');
    $cursaData  = [
      'estudiante_id' => $estudiante->id,
      'curso_id'      => $curso->id,
      'admin_id'      => $admin->id,
      'fecha'         => $fecha
    ];

    $cursa = Cursa::create($cursaData);
    $resp  = Resp::okWhitToken(
      'Estudiante agregado',
      'Estudiante Inscrito Correctamente',
      Utils::generateTokenFromAdmin($admin),
      null
    );
    return $resp;
  }
  public static function inscribeAntiguo($admin, $data) {
    $curso = Curso::find($data['datos']['curso_id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe el Curso con id: ' . $data['datos']['curso_id']);
      return $resp;
    }
    $est = Estudiante::find($data['datos']['estudiante_id']);
    if (!$est) {
      $resp = Resp::badRequest('No existe el Estudiante con id: ' . $data['datos']['estudiante_id']);
      return $resp;
    }
    $fecha     = date('Y-m-d');
    $cursaData = [
      'estudiante_id' => $est->id,
      'curso_id'      => $curso->id,
      'admin_id'      => $admin->id,
      'fecha'         => $fecha
    ];
    $cursa     = Cursa::create($cursaData);
    $resp      = Resp::okWhitToken(
      'Estudiante antiguo agregado',
      'Estudiante antiguo Inscrito Correctamente',
      Utils::generateTokenFromAdmin($admin),
      null
    );
    return $resp;
  }
  public static function borrar($admin, $data) {
    $cursa = Cursa::find($data['id']);
    if (!$cursa) {
      $resp = Resp::badRequest('No existe registro de cursa con id: ' . $data['id']);
      return $resp;
    }
    $cursa->delete();
    $lista = EstudianteController::list($admin);
    return $lista;
  }
  public static function updateNotes($admin, $data) {
    $cursa = Cursa::find($data['cursa_id']);
    if (!$cursa) {
      $resp = Resp::badRequest('No existe cursa con id: ' . $data['cursa_id']);
      return $resp;
    }
    $curso = Curso::find($data['curso_id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe curso con id: ' . $data['curso_id']);
      return $resp;
    }
    $cursa->update($data['datos']);
    $lista = EstudianteController::listWithNotes($admin, ['id' => $curso->id]);
    return $lista;
  }

  public static function pdfReport($id) {
    $cursa = Cursa::with(['estudiante', 'curso.libro', 'curso.admin'])->where('id', $id)->first();
    return [
      'cursa' => $cursa
    ];
  }
}