<?php
namespace Controllers;

use \Models\Utils;
use \Models\Response as Resp;
use \Models\Nivel;

class NivelController
{
  public static function list($admin) {
    if ($admin->tipo != 'root') {
      $response = Resp::badRequest('Ver lista de niveles solo con token del root');
      return $response;
    }
    $niveles = Nivel::all();
    $resp    = Resp::okWhitToken(
      'Lista cargada',
      'Lista obtenida correctamente',
      Utils::generateTokenFromAdmin($admin),
      ['niveles' => $niveles]
    );
    return $resp;
  }

  public static function update($admin, $data) {
    $nivel = Nivel::find($data['id']);
    if (!$nivel) {
      $resp = Resp::badRequest('No se encontro el nivel con id: ' . $data['id']);
      return $resp;
    }
    $nivel->update($data['datos']);
    $lista = self::list($admin);
    return $lista;
  }
}