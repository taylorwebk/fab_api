<?php
namespace Controllers;

use \Models\Utils;
use \Models\Response as Resp;
use \Models\Estudiante;
use \Models\Cursa;
use \Models\Curso;

class EstudianteController
{
  public static function consulta ($admin, $data) {
    $est = Estudiante::where('ci', $data['datos']['ci'])->first();
    if (!$est) {
      $resp = Resp::okWhitToken(
        'No se encontro el estudiante',
        'El C.I. ingresado no esta registrado',
        Utils::generateTokenFromAdmin($admin),
        ['estudiante' => null]
      );
      return $resp;
    }
    $estWithFullData = Estudiante::with('cursas.curso.libro')->where('ci', $est->ci)->first();
    $cursas          = $estWithFullData->cursas;
    $cursoActual     = $cursas->filter(
      function($cursa) {
        return $cursa->curso->fcierre == null;
      }
    )->first();
    if ($cursoActual) {
      $est->notas  = [
        'atencion'      => $cursoActual->natencion,
        'habla'         => $cursoActual->nhabla,
        'participacion' => $cursoActual->npart,
        'examen'        => $cursoActual->nexamen
      ];
      $est->libro  = $cursoActual->curso->libro->nro;
      $est->estado = 'Cursando';
    } else {
      $cursosAprobados = $cursas->filter(
        function ($cursa) {
          if ($cursa->curso->fcierre == null) {
            return false;
          }
          return true;
        }
      );
      $ultimoAprobado  = $cursosAprobados->where('curso_id', $cursosAprobados->max('curso_id'))->first();
      if ($ultimoAprobado) {
        $est->notas = [
          'atencion'      => $ultimoAprobado->natencion,
          'habla'         => $ultimoAprobado->nhabla,
          'participacion' => $ultimoAprobado->npart,
          'examen'        => $ultimoAprobado->nexamen
        ];
        $est->libro = $ultimoAprobado->curso->libro->nro;
        $notaF      = $ultimoAprobado->natecion + $ultimoAprobado->nhabla + $ultimoAprobado->npart + $ultimoAprobado->nexamen;

        $est->estado = $notaF >= 70 ? 'Aprobado' : 'Reprobado';
      } else {
        $est->notas  = null;
        $est->estado = null;
        $est->libro  = null;
      }
    }
    $resp = Resp::okWhitToken(
      'Estudiante encontrado',
      'Estudiante Encontrado',
      Utils::generateTokenFromAdmin($admin),
      ['estudiante' => $est]
    );    
    return $resp;
  }

  public static function update($admin, $data) {
    $est = Estudiante::find($data['id']);
    if (!$est) {
      $resp = Resp::badRequest('No existe el estudiante con id: ' . $data['id']);
    }
    $est->update($data['datos']);
    $lista = self::list($admin);
    return $lista;
  }

  public static function list($admin) {
    $cursos = Curso::with(['cursas.estudiante', 'libro'])
              ->where('sucursal_id', $admin->sucursal_id)
              ->where('fcierre', null)->get();
    
    $listaCursos = $cursos->map(
      function ($curso) {
        $nuevoCurso = [];

        $nuevoCurso['id']  = $curso->id;
        $nuevoCurso['nro'] = $curso->libro->nro;

        $nuevoCurso['estudiantes'] = $curso->cursas->map(
          function ($cursa) {
            $estudiante = [
              'id'        => $cursa->estudiante->id,
              'sexo'      => $cursa->estudiante->sexo,
              'correo'    => $cursa->estudiante->correo,
              'cursa_id'  => $cursa->id,
              'nombres'   => $cursa->estudiante->nombres,
              'apellidos' => $cursa->estudiante->apellidos,
              'ci'        => $cursa->estudiante->ci,
              'cel'       => $cursa->estudiante->cel
            ];
            return $estudiante;
          }
        );
        return $nuevoCurso;
      }
    );

    $resp = Resp::okWhitToken(
      'Lista obtenida',
      'Lista de inscritos cargada.',
      Utils::generateTokenFromAdmin($admin),
      ['cursos' => $listaCursos]
    );
    return $resp;
  }

  public static function listWithNotes($admin, $data) {
    $curso = Curso::find($data['id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe el Curso con id: ' . $data['id']);
      return $resp;
    }
    $cursoConLista = Curso::with(['cursas.estudiante', 'libro', 'admin'])
                  ->where('sucursal_id', $admin->sucursal_id)
                  ->where('id', $data['id'])->first();
    $dnombres      = '';
    $dapellidos    = '';
    if ($cursoConLista->admin) {
      $dnombres   = $cursoConLista->admin->nombres;
      $dapellidos = $cursoConLista->admin->apellidos; 
    } else {
      $dnombres   = 'Sin Asignar';
      $dapellidos = 'Sin Asignar';
    }
    $respCurso     = [
      'id'      => $cursoConLista->id,
      'faper'   => $cursoConLista->faper,
      'libro'   => $cursoConLista->libro,
      'docente' => [
        'nombres'   => $dnombres,
        'apellidos' => $dapellidos
      ],
      'estudiantes' => $cursoConLista->cursas->map(
        function($cursa) {
          $est = [
            'id'        => $cursa->estudiante->id,
            'cursa_id'  => $cursa->id,
            'natencion' => $cursa->natencion,
            'nhabla'    => $cursa->nhabla,
            'npart'     => $cursa->npart,
            'nexamen'   => $cursa->nexamen,
            'nombres'   => $cursa->estudiante->nombres,
            'apellidos' => $cursa->estudiante->apellidos,
            'ci'        => $cursa->estudiante->ci,
            'cel'       => $cursa->estudiante->cel
          ];
          return $est;
        }
      )
    ];

    $resp = Resp::okWhitToken(
      'Lista obtenida',
      'Lista cargada.',
      Utils::generateTokenFromAdmin($admin),
      ['curso' => $respCurso]
    );
    return $resp;
  }

  public static function notas($data) {
    $est = Estudiante::with(
      [
        'cursas.curso.libro', 'cursas.curso.admin'
      ]
    )->where('ci', $data['ci'])->first();
    
    if (!$est) {
      return Resp::unauthorized(
        'No existe el estudiante con ci: ' . $data['ci'],
        'No existe el estudiante con ci: ' . $data['ci']
      );
    }
    $cursos = [];
    if ($est->cursas) {
      $cursos = $est->cursas->map(
        function ($cursa) {
          $curso = [
            'atencion'      => $cursa->natencion,
            'habla'         => $cursa->nhabla,
            'participacion' => $cursa->nparticipacion,
            'examen'        => $cursa->nexamen,
            'libro'     => $cursa->curso->libro->nro,
            'apertura'  => $cursa->curso->faper,
            'cierre'    => $cursa->curso->fcierre,
            'docente'   => $cursa->curso->admin->nombres . ' ' . $cursa->curso->admin->apellidos
          ];
          return $curso;
        }
      );
    }
    $estResp = [
      'nombres'     => $est->nombres,
      'apellidos'   => $est->apellidos,
      'ci'          => $est->ci,
      'cursos'      => $cursos
    ];
    $resp    = Resp::ok(
      'Datos cargados',
      'Estudiante encontrado',
      ['estudiante' => $estResp]
    );
    return $resp;
  }

}
