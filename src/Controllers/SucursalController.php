<?php

namespace Controllers;

use \Models\Utils;
use \Models\Response as Resp;
use \Models\Sucursal;

class SucursalController
{
  public static function list($admin) {
    if ($admin->tipo != 'root') {
      $response = Resp::badRequest('Ver lista de sucursales solo con token del root');
      return $response;
    }
    $sucursales = Sucursal::all();
    $resp       = Resp::okWhitToken(
      'Lista obtenida',
      'Lista obtenida correctamente',
      Utils::generateToken($admin->id, $admin->correo, $admin->tipo),
      ['sucursales' => $sucursales]
    );
    return $resp;
  }
  public static function update($admin, $data) {
    $sucursal = Sucursal::find($data['id']);
    if (!$sucursal) {
      $resp = Resp::badRequest('No existe la sucursal con ID: ' . $data['id']);
      return $resp;
    }
    $sucursal->update($data['datos']);
    $resp = self::list($admin);
    return $resp;
  }
}