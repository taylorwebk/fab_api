<?php
namespace Controllers;

use \Models\Response as Resp;
use \Models\Utils;
use \Models\Root;
use \Models\Admin;
use \Models\Libro;
use \Models\Sucursal;

class AdminController
{
  public static function login($data) {
    $admin = Root::where('correo', $data['correo'])->first();
    if(!$admin) {
      $admin = Admin::where('correo', $data['correo'])->first();
    } else {
      $admin->tipo = 'root';
    }
    if (!$admin) {
      $resp = Resp::unauthorized('Correo no encontrado', 'Verifique sus datos e intente de nuevo por favor');
      return $resp;
    }
    if (!password_verify($data['password'], $admin->password)) {
      $resp = Resp::unauthorized(
        'password inválido.',
        'Verifique sus datos e intente de nuevo por favor.'
      );
      return $resp;
    }
    $respData = [
      'user' => $admin
    ];
    /* NO ES NECESARIO RESPUESTAS ADICIONALES EN EL LOGIN 
    switch ($admin->tipo) {
      case 'root':
        $respData['administrativos'] = Admin::where('tipo', 'administrativo')->get();
        break;
      case 'administrativo':
        $respData['libros'] = Libro::all();
      default:
        break;
    } */
    $resp = Resp::okWhitToken(
      'Login correcto',
      'Bienvenido ' . $admin->nombres,
      Utils::generateToken($admin->id, $admin->correo, $admin->tipo),
      $respData
    );
    return $resp;
  }

  // data['datos'] puede tener nombres, apellidos, dir, telf, cel, correo, sexo
  // $tipo puede ser administrativo o docente
  public static function add($admin, $data, $tipo) {
    if ($tipo == 'administrativo' && $admin->tipo != 'root') {
      $response = Resp::badRequest('Registrar nuevo administrativo solo desde root');
      return $response;
    }
    if ($tipo == 'docente' && $admin->tipo != 'administrativo') {
      $response = Resp::badRequest('Registrar nuevo docente solo desde admin, no root');
      return $response;
    }
    if ($admin->correo == $data['datos']['correo']) {
      $resp = Resp::unauthorized(
        'correo enviado es igual que el correo del admin',
        'El correo ya esta siendo utilizado'
      );
      return $resp;
    }
    $data['datos']['password'] = password_hash($data['datos']['correo'], PASSWORD_DEFAULT);
    $data['datos']['tipo']     = $tipo;

    if (array_key_exists('sucursal_id', $data['datos'])) {
      $sucursal = Sucursal::find($data['datos']['sucursal_id']);
      if (!$sucursal) {
        $resp = Resp::badRequest('No existe la sucursal con id: ' . $data['datos']['sucursal_id']);
        return $resp;
      }
    } else {
      $data['datos']['sucursal_id'] = $admin->sucursal_id;
    }
    
    Admin::create($data['datos']);
    $lista = [];
    if ($tipo == 'administrativo') {
      $lista = [
        'administrativos' => Admin::where('tipo', 'administrativo')->get()
      ];
    }
    if ($tipo == 'docente') {
      $lista = [
        'docentes' => Admin::where('tipo', 'docente')->get()
      ];
    }
    $resp = Resp::okWhitToken(
      'todo OK, revisar respuesta, devolviendo lista modificada',
      'Nuevo registro creado',
      Utils::generateToken($admin->id, $admin->correo, $admin->tipo),
      $lista
    );
    return $resp;
  }

  public static function update($admin, $data) {
    if (array_key_exists('correo', $data['datos'])) {
      $checkAdmin = Admin::where('correo', $data['datos']['correo'])->first();
      if ($checkAdmin || $admin->correo == $data['datos']['correo']) {
        $resp = Resp::unauthorized(
          'correo es igual que el correo de algun admin o docente (si se quiere mantener algun dato, no enviarlo)',
          'El correo ya esta siendo utilizado'
        );
        return $resp;
      }
    }
    $adminResult = Admin::find($data['id']);
    if (!$adminResult) {
      $resp = Resp::badRequest('Id ' . $data['id'] . ' no ha sido encontrado');
      return $resp;
    }
    if (array_key_exists('password', $data['datos'])) {
      $data['datos']['password'] = password_hash($data['datos']['password'], PASSWORD_DEFAULT);
    }
    if (array_key_exists('sucursal_id', $data['datos'])) {
      $sucursal = Sucursal::find($data['datos']['sucursal_id']);
      if (!$sucursal) {
        $resp = Resp::badRequest('No existe la sucursal con id: ' . $data['datos']['sucursal_id']);
        return $resp;
      }
    }
    $adminResult->update($data['datos']);
    $lista = [];
    if ($admin->tipo == 'root') {
      $lista = [
        'administrativos' => Admin::where('tipo', 'administrativo')->get()
      ];
    }
    if ($admin->tipo == 'administrativo') {
      $lista = [
        'docentes' => Admin::where('tipo', 'docente')->get()
      ];
    }
    $resp = Resp::okWhitToken(
      'En teoria,todo bien, devolviendo nueva lista',
      'Datos actualizados correctamente',
      Utils::generateToken($admin->id, $admin->correo, $admin->tipo),
      $lista
    );
    return $resp;
  }

  public static function deleteAdmin($admin, $data) {
    $adminResult = Admin::find($data['id']);
    if (!$adminResult) {
      $resp = Resp::badRequest('Id ' . $data['id'] . ' no ha sido encontrado');
      return $resp;
    }
    $adminResult->delete();

    $resp = Resp::okWhitToken(
      'administrativo borrado, devolviendo nueva lista',
      'Administrativo borrado',
      Utils::generateTokenFromAdmin($admin),
      ['administrativos' => Admin::where('tipo', 'administrativo')->get()]
    );
    return $resp;
  }

  public static function listAdmins($admin) {
    if ($admin->tipo != 'root') {
      $response = Resp::badRequest('Lista de admins solo con token del root');
      return $response;
    }
    $resp = Resp::okWhitToken(
      'Lista de admins',
      'Lista de Administrativos Cargada.',
      Utils::generateTokenFromAdmin($admin),
      ['administrativos' => Admin::where('tipo', 'administrativo')->get()]
    );
    return $resp;
  }

  public static function listDocs($admin) {
    if ($admin->tipo != 'administrativo') {
      $response = Resp::badRequest('Lista de docentes solo con token del administrativo');
      return $response;
    }
    $resp = Resp::okWhitToken(
      'Lista de docentes',
      'Lista de Docentes Cargada.',
      Utils::generateTokenFromAdmin($admin),
      ['docentes' => Admin::select(
        'id', 'nombres', 'apellidos', 'dir', 'telf', 'cel', 'correo', 'sexo'
      )->where('tipo', 'docente')->where('sucursal_id', $admin->sucursal_id)->get()]
    );
    return $resp;
  }

  public static function listCourses($admin) {
    $adminData = Admin::with(
      ['cursos' => function ($query) {
        $query->where('fcierre', null);
      }, 'cursos.libro']
    )
    ->where('tipo', 'docente')
    ->where('id', $admin->id)->first();
    $cursos    = $adminData->cursos->map(
      function($curso) {
        $cursoItem = [
          'id'    => $curso->id,
          'libro' => [
            'nro' => $curso->libro->nro
          ]
        ];
        return $cursoItem;
      }
    );
    $resp      = Resp::okWhitToken(
      'Lista de cursos obtenida',
      'Lista de cursos obtenida',
      Utils::generateTokenFromAdmin($admin),
      ['cursos' => $cursos]
    );
    return $resp;
  }
}
