<?php
namespace Controllers;

use \Models\Response as Resp;
use \Models\Utils;
use \Models\Curso;
use \Models\Admin;

class CursoController
{
  public static function list($admin) {
    if ($admin->tipo != 'administrativo') {
      $resp = Resp::badRequest('Lista de cursos solo con el token del administrativo');
      return $resp;
    }
    $listaHab   = Curso::with(['admin:id,nombres,apellidos', 'libro:id,nro'])
      ->where('habilitado', 1)->where('fcierre', null)->where('sucursal_id', $admin->sucursal_id)->get();
    $listaInhab = Curso::with(['admin:id,nombres,apellidos', 'libro:id,nro'])
      ->where('habilitado', 0)->where('fcierre', null)->where('sucursal_id', $admin->sucursal_id)->get();
    $resp       = Resp::okWhitToken(
      'Lista obtenida',
      'Lista cargada',
      Utils::generateTokenFromAdmin($admin),
      ['cursos' => [
        'habilitados' => $listaHab,
        'inhabilitados' => $listaInhab
      ]]
    );
    return $resp;
  }
  public static function listClosed($admin) {
    if ($admin->tipo != 'administrativo') {
      $resp = Resp::badRequest('Lista de cursos solo con el token del administrativo');
      return $resp;
    }
    $lista = Curso::with(['admin:id,nombres,apellidos', 'libro:id,nro'])
      ->where('fcierre', '!=', null)->where('sucursal_id', $admin->sucursal_id)->get();
    $resp  = Resp::okWhitToken(
      'Lista obtenida',
      'Lista cargada',
      Utils::generateTokenFromAdmin($admin),
      ['cursos' => $lista]
    );
    return $resp;
  }
  // En data enviar: libro_id, admin_id (profesor id)
  public static function add($admin, $data) {
    if ($admin->tipo != 'administrativo') {
      $resp = Resp::badRequest('Agregar curso solo con el token del administrativo');
      return $resp;
    }
    if (array_key_exists('admin_id', $data['datos'])) {
      $doc = Admin::find($data['datos']['admin_id']);
      if (!$doc) {
        $resp = Resp::badRequest('No existe el docente con id: ' . $data['datos']['admin_id']);
        return $resp;
      } else {
        if ($doc->tipo != 'docente') {
          $resp = Resp::badRequest('El Id enviado no es de un docente, es de un admin: ' . $data['datos']['admin_id']);
          return $resp;
        }
      }
    }
    $fecha = date('Y-m-d');

    $data['datos']['sucursal_id'] = $admin->sucursal_id;
    $data['datos']['faper']       = $fecha;
    $data['datos']['habilitado']  = true;
    Curso::create($data['datos']);
    $lista = self::list($admin);
    return $lista;
  }
  // Solo se deberia modificar el admin_id
  public static function update($admin, $data) {
    if ($admin->tipo != 'administrativo') {
      $resp = Resp::badRequest('Modificar curso solo con el token del administrativo');
      return $resp;
    }
    $curso = Curso::find($data['id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe el curso con id: ' . $data['id']);
    }
    $doc = Admin::find($data['datos']['admin_id']);
    if (!$doc) {
      $resp = Resp::badRequest('No existe el docente con id: ' . $data['id']);
      return $resp;
    }
    $curso->admin_id = $data['datos']['admin_id'];
    $curso->save();
    $lista = self::list($admin);
    return $lista;
  }

  public static function disable($admin, $data) {
    if ($admin->tipo != 'administrativo') {
      $resp = Resp::badRequest('Inhabilitar curso solo con el token del administrativo');
      return $resp;
    }
    $curso = Curso::find($data['id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe el curso con id: ' . $data['id']);
      return $resp;
    }
    $curso->habilitado = false;
    $curso->save();
    $lista = self::list($admin);
    return $lista;
  }

  public static function enable($admin, $data) {
    if ($admin->tipo != 'administrativo') {
      $resp = Resp::badRequest('Habilitar curso solo con el token del administrativo');
      return $resp;
    }
    $curso = Curso::find($data['id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe el curso con id: ' . $data['id']);
      return $resp;
    }
    $curso->habilitado = true;
    $curso->save();
    $lista = self::list($admin);
    return $lista;
  }

  public static function close($admin, $data) {
    if ($admin->tipo != 'administrativo') {
      $resp = Resp::badRequest('Cerrar curso solo con el token del administrativo');
      return $resp;
    }
    $curso = Curso::find($data['id']);
    if (!$curso) {
      $resp = Resp::badRequest('No existe el curso con id: ' . $data['id']);
      return $resp;
    }
    $fecha = date('Y-m-d');

    $curso->fcierre    = $fecha;
    $curso->habilitado = false;
    $curso->save();
    $lista = self::list($admin);
    return $lista;
  }

  public static function pdfReport($id) {
    $curso = Curso::with(['cursas.estudiante', 'admin', 'libro'])->where('id', $id)->first();
    return [
      'curso' => $curso
    ];
  }
}