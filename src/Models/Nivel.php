<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
  protected $guarded = [];
  protected $table   = 'nivel';
  public $timestamps = false;
}
