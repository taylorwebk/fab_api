<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Root extends Model
{
  protected $guarded = [];
  protected $table   = 'root';
  protected $hidden  = ['password', 'id'];
  public $timestamps = false;
}
