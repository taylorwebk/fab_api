<?php
namespace Models;

use \Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
  protected $guarded = [];
  protected $table   = 'curso';
  protected $hidden  = ['libro_id', 'sucursal_id', 'admin_id'];
  public $timestamps = false;

  public function admin() {
    return $this->belongsTo('\Models\Admin');
  }
  public function libro() {
    return $this->belongsTo('\Models\Libro');
  }
  public function cursas() {
    return $this->hasMany('\Models\Cursa');
  }
}