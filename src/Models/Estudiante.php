<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
  protected $guarded = [];
  protected $table   = 'estudiante';
  public $timestamps = false;

  public function cursas() {
    return $this->hasMany('\Models\Cursa');
  }
}
