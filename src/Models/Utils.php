<?php
namespace Models;

use \Lcobucci\JWT\Builder;
use \Lcobucci\JWT\ValidationData;
use \Lcobucci\JWT\Parser;

use \Models\Root;

class Utils
{
  /**
   * Generates a token based in a id and ci of user.
   */
  public static function generateToken($id, $correo, $tipo) {
    $currentTime = time();
    $tokenstr    = (new Builder())
      ->setIssuer(IP)
      ->setIssuedAt($currentTime)
      ->setExpiration($currentTime + 90 * 60 * 24)
      ->set('id', $id)
      ->set('correo', $correo)
      ->set('tipo', $tipo)
      ->getToken();
    return (string)$tokenstr;
  }

  public static function generateTokenFromAdmin($admin) {
    $currentTime = time();
    $tokenstr    = (new Builder())
      ->setIssuer(IP)
      ->setIssuedAt($currentTime)
      ->setExpiration($currentTime + 90 * 60 * 24)
      ->set('id', $admin->id)
      ->set('correo', $admin->correo)
      ->set('tipo', $admin->tipo)
      ->getToken();
    return (string)$tokenstr;
  }

  public static function decodeToken($tokenstr) {
    $token = (new Parser())->parse((string)$tokenstr);
    if (!$token->isExpired()) {
      $returArray = [
        'id' => $token->getClaim('id'),
        'correo' => $token->getClaim('correo'),
        'tipo' => $token->getClaim('tipo')
      ];
      return $returArray; 
    }
    return false;
  }

  public static function getRoot() {
    $root = Root::first();
    return $root;
  }

  public static function inMultiarray($elem, $array, $key) {
    $top    = count($array) - 1;
    $bottom = 0;
    while ($bottom <= $top) {
      if ($array[$bottom][$key] == $elem) {
        return true;
      }
      $bottom++;
    }
    return false;
  }
  public static function toPdf($mpdf, $res, $url) {
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
    $html     = \file_get_contents($protocol . IP . '/fab_api' . $url);
    $mpdf->writeHtml($html);
    $res  = $res->withHeader('Content-type', 'application/pdf');
    $body = $res->getBody();
    $body->write($mpdf->output());
    return $res;
  }
}
