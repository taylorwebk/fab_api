<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
  protected $guarded = [];
  protected $table   = 'sucursal';
  public $timestamps = false;
}
