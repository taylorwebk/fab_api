<?php
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
  protected $guarded = [];
  protected $table   = 'libro';
  protected $hidden  = ['nivel_id'];
  public $timestamps = false;
}
