drop database if exists fab;
CREATE DATABASE fab DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
use fab;

create table root(
  id integer not null auto_increment,
  nombres varchar(127),
  apellidos varchar(127),
  correo varchar(127),
  password varchar(255),
  primary key(id)
);
create table estudiante(
  id integer not null auto_increment,
  nombres varchar(127),
  apellidos varchar(127),
  ci varchar(127),
  sexo enum('M', 'F'),
  cel varchar(127),
  correo varchar(127),
  primary key(id)
);
create table sucursal(
  id integer not null auto_increment,
  dir varchar(255),
  nombre varchar(127),
  primary key(id)
);
create table admin(
  id integer not null auto_increment,
  sucursal_id integer null,
  nombres varchar(127),
  apellidos varchar(127),
  dir varchar(255),
  telf varchar(127),
  cel varchar(127),
  correo varchar(127),
  sexo enum('M', 'F'),
  tipo enum('administrativo', 'docente'),
  password varchar(255),
  primary key(id),
  foreign key(sucursal_id)
  references sucursal(id)
);
create table nivel(
  id integer not null auto_increment,
  nombre varchar(127),
  notamin integer,
  primary key(id)
);
create table libro(
  id integer not null auto_increment,
  nivel_id integer not null,
  nro tinyint,
  primary key(id),
  foreign key(nivel_id)
  references nivel(id)
);
create table curso(
  id integer not null auto_increment,
  libro_id integer not null,
  sucursal_id integer not null,
  admin_id integer null,
  faper date,
  fcierre date,
  habilitado boolean,
  primary key(id),
  foreign key(libro_id)
  references libro(id)
  on delete cascade,
  foreign key(sucursal_id)
  references sucursal(id)
  on delete cascade,
  foreign key(admin_id)
  references admin(id)
  on delete cascade
);
create table cursa(
  id integer not null auto_increment,
  estudiante_id integer not null,
  curso_id integer not null,
  admin_id integer null,
  fecha date,
  natencion tinyint,
  nhabla tinyint,
  npart tinyint,
  nexamen tinyint,
  fexamen date,
  primary key(id),
  foreign key(estudiante_id)
  references estudiante(id),
  foreign key(curso_id)
  references curso(id),
  foreign key(admin_id)
  references admin(id)
);
create table cursa_nivel(
  nivel_id integer not null,
  cursa_id integer not null,
  notamin tinyint,
  nota tinyint,
  obs varchar(127),
  primary key(nivel_id, cursa_id),
  foreign key(nivel_id)
  references nivel(id),
  foreign key(cursa_id)
  references cursa(id)
);
insert into root values(null, 'root name', 'root lastname', 'root@root.com', '$2y$10$gU675UtNDxX7hLBr5DE3feqxvn9v1OGpJyNIa623La1G55HIRTSva');
insert into sucursal values(null, 'Escuela de Idiomas de la Fuerza Aérea Boliviana', 'Calle x defecto, Avenida inventada');
insert into nivel values
(null, 'Nivel 1', 25),
(null, 'Nivel 2', 49),
(null, 'Nivel 3', 60),
(null, 'Nivel 4', 70),
(null, 'Nivel 5', 80),
(null, 'Nivel 6', 85);
insert into libro values
(null, 1, 1), (null, 1, 2), (null, 1, 3), (null, 1, 4), (null, 1, 5), (null, 1, 6),
(null, 2, 7), (null, 2, 8), (null, 2, 9), (null, 2, 10), (null, 2, 11), (null, 2, 12),
(null, 3, 13), (null, 3, 14), (null, 3, 15), (null, 3, 16), (null, 3, 17), (null, 3, 18),
(null, 4, 19), (null, 4, 20), (null, 4, 21), (null, 4, 22), (null, 4, 23), (null, 4, 24),
(null, 5, 25), (null, 5, 26), (null, 5, 27), (null, 5, 28), (null, 5, 29), (null, 5, 30),
(null, 6, 31), (null, 6, 32), (null, 6, 33), (null, 6, 34);