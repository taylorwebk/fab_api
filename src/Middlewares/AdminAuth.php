<?php

/**
 * Middleware para validar el token en cada peticion
 */
namespace Middlewares;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Models\Utils;
use \Models\Admin;
use \Models\Root;
use \Models\Response as Resp;

class AdminAuth
{
  public function __invoke(Request $req, Response $res, $next) {
    $tokenarr = $req->getHeader('Authorization');
    if (count($tokenarr) == 0) {
      $response = $res->withJson(Resp::badRequest('No se reconoce el token en los headers.'));
      return $response;
    }
    $data = Utils::decodeToken($tokenarr[0]);
    if ($data === false) {
      $response = Resp::unauthorized(
        'Token expirado.',
        'Ha estado inactivo por un largo tiempo, por favor ingrese de nuevo'
      );
      $response = $res->withJson($response);
      return $response;
    }
    //var_dump($next['logger']);
    $admin = Admin::where('correo', $data['correo'])->first();
    if(!$admin) {
      $admin = Root::where('correo', $data['correo'])->first();
      if($admin) {
        $admin->tipo = 'root';
      }
    }
    if (!$admin) {
      $response = Resp::badRequest('Error con el Token');
      $response = $res->withJson($response);
      return $response;
    }
    $req = $req->withAttribute('admin', $admin);
    $res = $next($req, $res);
    // HERE GOES THE ACTIONS AFTER THE REQUEST
    return $res;
  }
}
