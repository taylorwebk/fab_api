<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Models\Utils;

use \Controllers\AdminController as AdminC;
use \Controllers\RootController as RootC;
use \Controllers\SucursalController as SucC;
use \Controllers\NivelController as NivelC;
use \Controllers\CursoController as CursoC;
use \Controllers\LibroController as LibroC;
use \Controllers\EstudianteController as EstudianteC;
use \Controllers\CursaController as CursaC;

use \Middlewares\AdminAuth;

$app->get(
  '/',
  function (Request $req, Response $res) {
    $resp = $res->withJson(password_hash('123456', PASSWORD_DEFAULT));
    return $resp;
  }
);

$app->post(
  '/login',
  function (Request $req, Response $res){
    $result = $res->withJson(AdminC::login($req->getParsedBody()));
    return $result;
  }
);

$app->group(
  '/root',
  function () use ($app) {

    $app->put(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = RootC::update($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
  
  }
)->add(new AdminAuth());

$app->group(
  '/admin',
  function () use ($app) {

    $app->get(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::listAdmins($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::add($admin, $req->getParsedBody(), 'administrativo');
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->put(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::update($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->delete(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::deleteAdmin($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
  
  }
)->add(new AdminAuth());

$app->group(
  '/sucursal',
  function () use ($app) {

    $app->get(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = SucC::list($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->put(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = SucC::update($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
  
  }
)->add(new AdminAuth());

$app->group(
  '/nivel',
  function () use ($app) {

    $app->get(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = NivelC::list($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->put(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = NivelC::update($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
  
  }
)->add(new AdminAuth());

$app->group(
  '/docente',
  function () use ($app) {

    $app->get(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::listDocs($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->get(
      '/cursos',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::listCourses($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::add($admin, $req->getParsedBody(), 'docente');
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->put(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = AdminC::update($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );

  }
)->add(new AdminAuth());

$app->group(
  '/curso',
  function () use ($app) {

    $app->get(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursoC::list($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->get(
      '/historial',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursoC::listClosed($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursoC::add($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->put(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursoC::update($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/inhabilitar',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursoC::disable($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/habilitar',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursoC::enable($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/cerrar',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursoC::close($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );

  }
)->add(new AdminAuth());

$app->group(
  '/libro',
  function () use ($app) {

    $app->get(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = LibroC::list($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );

  }
)->add(new AdminAuth());

$app->group(
  '/estudiante',
  function () use ($app) {

    $app->post(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = EstudianteC::update($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/inscribe',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursaC::inscribe($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/inscribeantiguo',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursaC::inscribeAntiguo($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/notas',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = EstudianteC::listWithNotes($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/nota',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursaC::updateNotes($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/consulta',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = EstudianteC::consulta($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->get(
      '/',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = EstudianteC::list($admin);
        $response = $res->withJson($result);
        return $response;
      }
    );
    $app->post(
      '/borrar',
      function (Request $req, Response $res) {
        $admin    = $req->getAttribute('admin');
        $result   = CursaC::borrar($admin, $req->getParsedBody());
        $response = $res->withJson($result);
        return $response;
      }
    );

  }
)->add(new AdminAuth());

$app->group(
  '/reporteo',
  function () use ($app) {

    $app->get(
      '/curso/{id:[0-9]+}',
      function (Request $req, Response $res, $args) {
        $data   = CursoC::pdfReport($args['id']);
        $result = $this->view->render($res, 'Curso.phtml', $data);
        return $result;
      }
    );
    $app->get(
      '/cursa/{id:[0-9]+}',
      function (Request $req, Response $res, $args) {
        $data   = CursaC::pdfReport($args['id']);
        $result = $this->view->render($res, 'Cursa.phtml', $data);
        return $result;
      }
    );

  }
);
$app->group(
  '/reporte',
  function () use ($app) {

    $app->get(
      '/curso/{id:[0-9]+}',
      function (Request $req, Response $res, $args) {
        $data = Utils::toPdf($this->mpdf, $res, '/reporteo/curso/' . $args['id']);
        return $data;
      }
    );
    $app->get(
      '/cursa/{id:[0-9]+}',
      function (Request $req, Response $res, $args) {
        $data = Utils::toPdf($this->mpdf, $res, '/reporteo/cursa/' . $args['id']);
        return $data;
      }
    );

  }
);
$app->post(
  '/notas',
  function (Request $req, Response $res) {
    $result   = EstudianteC::notas($req->getParsedBody());
    $response = $res->withJson($result);
    return $response;
  }
);
